package main

import "gitlab.com/tluke12345/go-server/pkg/server"

func main() {
	// Call the function that will start the server
	server.StartServer()
}
