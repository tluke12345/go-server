# go-server

NOTE: Still planning phase

TODO:

GENERAL

- [ ] obtain and deploy to server
- [ ] determine how to automate deployment with ci/cd
- [ ] Logging/monitoring?

WEB

- [x] auth login
- [ ] auth db
- [ ] auth registration?
- [ ] auth logout?
- [ ] port over public blog
- [ ] implement logged in user blog site
- [ ] port over public keyboard page
- [ ] implement logged in keyboard page (manage keyboards/keymaps)
- [ ] port over typing page
- [ ] implement logged in typing functionality (cloud sync and history management)
- [ ] implement calories page

ADMIN

- [ ] implement admin home
- [ ] implement blog admin
- [ ] implement keyboard admin
- [ ] implement keymap admin
- [ ] implement typing admin
- [ ] implement calories admin

API

- [ ] implement oauth2 server
- [ ] implement blog api
- [x] implement keyboard api
- [x] implement keymap api
- [ ] implement typing api
- [ ] implement typing history api
- [ ] implement calories api

## Description

This is the server for <www.travis-luke.com>. It serves many purposes, including:

- serving my personal website, [www.travis-luke.com](https://www.travis-luke.com),
  which has pages for my personal projects.

  - /login (WIP) - TODO: want to understand oauth better so implement this
  - /blog (WIP) - TODO: port over my flask blog
  - /keyboard (WIP) - TODO: wire up with the below API
  - /typing (WIP) - TODO: wire up with the below API, implement cloud
  - /calories (WIP) - TODO: everything

- api for my various projects

  - /api/openapi (WIP) - openapi spec for the api

  - oauth2 (WIP) - oauth2 server for logging in

    - /authorize
    - /oath2/token

  - /api/blogs (WIP) - actual text and images are on s3
  - /api/keyboards
  - /api/keymaps
  - /api/typing (WIP) - list of typing texts (actual text are on s3)
  - /api/calories (WIP)

The website is public and each project has its own page, but to save and access
data, you need to be logged in. The server uses oauth2 for authentication. I
implmented the oauth2 server myself to better understand how it works (don't
do this in real life!).

## Access (OAuth2)

[OAuth 2.0 rfc](https://tools.ietf.org/html/rfc6749)
[OAuth 2.0 Authorization Framework](https://auth0.com/docs/authenticate/protocols/oauth)

To access the server, you need to be logged in. The server uses oauth2 for
authentication. The server is the oauth2 server and the client is the website.

### Enpoints

/authorize

...

/oauth2/token

Used by the clients to get access and refresh tokens.

- "grant_type":
  - "authorization_code" to authorize (initial login)
  - "refresh_token" to refresh (after access token expires)

### The Oauth flow

## API

set the openapi spec for details: [openapi](/api/openapi)

/api/blogs

- GET /api/blogs - list of blog summaries
- GET /api/blogs/{id} - get a blog by id
- POST /api/blogs - create a new blog
- PUT /api/blogs/{id} - update a blog by id
- DELETE /api/blogs/{id} - delete a blog by id

/api/keyboards

- GET /api/keyboards - list of keyboards
- GET /api/keyboards/{id} - get a keyboard by id
- POST /api/keyboards - create a new keyboard
- PUT /api/keyboards/{id} - update a keyboard by id
- DELETE /api/keyboards/{id} - delete a keyboard by id

/api/keymaps

- GET /api/keymaps - list of keymaps
- GET /api/keymaps/{id} - get a keymap by id
- POST /api/keymaps - create a new keymap
- PUT /api/keymaps/{id} - update a keymap by id
- DELETE /api/keymaps/{id} - delete a keymap by id

/api/typing

- GET /api/typing - list of typing texts
- GET /api/typing/{id} - get a typing text by id
- POST /api/typing - create a new typing text
- PUT /api/typing/{id} - update a typing text by id
- DELETE /api/typing/{id} - delete a typing text by id

- GET /api/typing/history/{userid} - get all typing history for user id
- PUT /api/typing/history/{userid} - set/update user typing history

/api/calories

- GET /api/calories/{userid} - get calorie history for user id
- POST /api/calories/{userid} - add calorie entry for user id
- PUT /api/calories/{userid}/{id} - update calorie entry for user id
- DELETE /api/calories/{userid}/{id} - delete calorie entry for user id

## Development

To running the server (restart required when you make changes)

    ```bash
    go run ./main.go 
    ```

Re-generating the templ files (regenerate required when changed)

    ```bash
    templ generate
    ```

To setup the development environment:

    ```bash
    scripts/setup.sh
    ```

To run the development server:

    ```bash
    go run main.go
    ```

To run the tests:

    ```bash
    go test -v ./...
    ```

## Deployment Details
