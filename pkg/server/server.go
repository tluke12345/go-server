package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/tluke12345/go-server/pkg/auth"
	"gitlab.com/tluke12345/go-server/pkg/fileServer"
	"gitlab.com/tluke12345/go-server/pkg/keyboard"
	"gitlab.com/tluke12345/go-server/pkg/keymap"
)

const (
	// The port that the server will listen on
	PORT = ":3001"
)

func panicIfErr(err error) {
	if err != nil {
		println(err)
		panic(err)
	}
}

func StartServer() {
	mux := chi.NewRouter()

	mux.Use(auth.ParseJWT)

	// Static files
	fileServer.NewRouter(mux, "/static/*", "web/static")

	// Auth pages
	mux.Mount("/auth", auth.AuthPagesRouter())

	// Keymaps
	keymapRepo, err := keymap.NewJsonKeymapRepository()
	panicIfErr(err)
	mux.Mount("/api/keymaps", keymap.NewKeymapRouter(keymapRepo))

	// Keyboards
	keyboardRepo, err := keyboard.NewJsonKeyboardRepository()
	panicIfErr(err)
	mux.Mount("/api/keyboards", keyboard.NewKeyboardRouter(keyboardRepo))

	// Start the server
	fmt.Println("Starting server on port", PORT)
	log.Fatal(http.ListenAndServe(PORT, mux))
}
