package keymap

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/metal3d/go-slugify"
)

const keymapDirectory = "data/keymaps"

// Json based keymap repository
type JsonKeymapRepository struct {
	// KeymapCollections is a map of keymap collections
	//   - key: collection slug
	//   - value: the collection
	KeymapCollections map[string]*KeymapCollection

	// KeymapNames is a list of keymap collection names
	//  - key: collection slug
	//  - value: collection name
	KeymapNames []*KeymapNames
}

// ----------------------------------------------------------------------------
// Implement KeymapRepository Interface
// ----------------------------------------------------------------------------
func (r *JsonKeymapRepository) GetKeymapNames() ([]*KeymapNames, error) {
	return r.KeymapNames, nil
}

func (r *JsonKeymapRepository) GetKeymapCollection(slug string) (*KeymapCollection, error) {
	collection, ok := r.KeymapCollections[slug]
	if !ok {
		return nil, fmt.Errorf("keymap collection with slug %s not found", slug)
	}
	return collection, nil
}

// ----------------------------------------------------------------------------
// Loading JSON Data
// ----------------------------------------------------------------------------
func loadKeymapFilenames() ([]string, error) {
	files, err := os.ReadDir(keymapDirectory)
	if err != nil {
		return nil, err
	}

	paths := []string{}
	for _, file := range files {
		paths = append(paths, fmt.Sprintf("%s/%s", keymapDirectory, file.Name()))
	}
	return paths, nil
}

func loadKeymapCollection(path string) (*KeymapCollection, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	var keymap KeymapCollection
	err = json.Unmarshal(byteValue, &keymap)
	if err != nil {
		return nil, err
	}

	keymap.Slug = slugify.Marshal(keymap.Name)
	return &keymap, nil
}

// ----------------------------------------------------------------------------
// Creating a (JSON based) Repository
// ----------------------------------------------------------------------------
func NewJsonKeymapRepository() (*JsonKeymapRepository, error) {
	keymapPaths, err := loadKeymapFilenames()
	if err != nil {
		return nil, err
	}

	collections := map[string]*KeymapCollection{}
	names := []*KeymapNames{}
	for _, path := range keymapPaths {
		collection, err := loadKeymapCollection(path)
		if err != nil {
			return nil, err
		}
		names = append(names, &KeymapNames{
			Name: collection.Name,
			Slug: collection.Slug,
			Url:  fmt.Sprintf("/keymaps/%s", collection.Slug),
		})
		collections[collection.Slug] = collection
	}
	return &JsonKeymapRepository{
		KeymapCollections: collections,
		KeymapNames:       names,
	}, nil
}
