package keymap

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
)

// ----------------------------------------------------------------------------
// Router
// ----------------------------------------------------------------------------
func NewKeymapRouter(r KeymapRepository) *chi.Mux {
	mux := chi.NewRouter()
	mux.Get("/{slug}", keymapCollectionHandler(r))
	mux.Get("/", keymapNamesHandler(r))
	return mux
}

// ----------------------------------------------------------------------------
// Handlers
// ----------------------------------------------------------------------------
func keymapNamesHandler(r KeymapRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		keymapNames, err := r.GetKeymapNames()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(keymapNames)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func keymapCollectionHandler(r KeymapRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		slug := chi.URLParam(req, "slug")
		if slug == "" {
			http.Error(w, "missing name query parameter", http.StatusBadRequest)
			return
		}

		keymapCollection, err := r.GetKeymapCollection(slug)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(keymapCollection)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}
