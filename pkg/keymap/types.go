package keymap

// Map of available keymap collection names
//   - key: slug
//   - value: name
type KeymapNames struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
	Url  string `json:"url"`
}

// KeymapCollection represents a collection of keymaps
//   - Name: the name of the collection
//   - Slug: the slug of the collection (used in the URL)
//   - Keymaps: a list of keymaps
type KeymapCollection struct {
	Name    string   `json:"name"`
	Slug    string   `json:"slug"`
	Keymaps []Keymap `json:"keymaps"`
}

// Keymap represents a single keymap
//   - Name: the name of the keymap
//   - Description: a description of the keymap
//   - Program: the program that the keymap is for
//   - Chords: a list of chords that describe the keymap
type Keymap struct {
	Name        string     `json:"name,omitempty"`
	Description string     `json:"description,omitempty"`
	Program     string     `json:"program,omitempty"`
	Chords      [][]string `json:"chords"`
}

// KeymapRepository is an interface for interacting with keymap data
//   - GetKeymapNames: returns a map of keymap names by slug
//   - GetKeymapCollection: returns a keymap collection by slug
type KeymapRepository interface {
	GetKeymapNames() ([]*KeymapNames, error)
	GetKeymapCollection(slug string) (*KeymapCollection, error)
}
