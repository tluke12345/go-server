package keyboard

// KeyboardNames is a map of keyboard names
//   - key: slug
//   - value: name
type KeyboardName struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
	Url  string `json:"url"`
}

// KeyboardCollection represents a collection of keyboards
//   - Name: the name of the collection
//   - Slug: the slug of the collection (used in the URL)
//   - Keyboards: a list of keyboards
type Keyboard struct {
	Name   string  `json:"name"`
	Slug   string  `json:"slug"`
	Layout [][]Key `json:"layout"`
}

// Key represents a single key
//   - Value: the value of the key
//   - ShiftValue: the value of the key when the shift key is pressed
//   - Width: the width of the key (for layout. 1: normal, 2: double, 0.5: half)
type Key struct {
	Value      string  `json:"value,omitempty"`
	ShiftValue string  `json:"shift,omitempty"`
	Width      float32 `json:"width,omitempty"`
}

// KeyboardRepository is an interface for interacting with keyboard data
//   - GetKeyboardNames: returns a list of keyboard names by slug
//   - GetKeyboard: returns a keyboard by slug
type KeyboardRepository interface {
	GetKeyboardNames() ([]*KeyboardName, error)
	GetKeyboard(slug string) (*Keyboard, error)
}
