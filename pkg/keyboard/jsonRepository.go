package keyboard

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"github.com/metal3d/go-slugify"
)

const keyboardDirectory = "data/keyboards"

// Json based keymap repository
type JsonKeyboardRepository struct {
	// KeymapCollections is a map of keymap collections
	//   - key: collection slug
	//   - value: the collection
	Keyboards map[string]*Keyboard

	// KeymapNames is a list of keymap collection names
	//  - key: collection slug
	//  - value: collection name
	KeyboardNames []*KeyboardName
}

// ----------------------------------------------------------------------------
// Implement KeymapRepository Interface
// ----------------------------------------------------------------------------
func (r *JsonKeyboardRepository) GetKeyboardNames() ([]*KeyboardName, error) {
	return r.KeyboardNames, nil
}

func (r *JsonKeyboardRepository) GetKeyboard(slug string) (*Keyboard, error) {
	collection, ok := r.Keyboards[slug]
	if !ok {
		return nil, fmt.Errorf("keymap collection with slug %s not found", slug)
	}
	return collection, nil
}

// ----------------------------------------------------------------------------
// Loading JSON Data
// ----------------------------------------------------------------------------
func loadKeymapFilenames() ([]string, error) {
	files, err := os.ReadDir(keyboardDirectory)
	if err != nil {
		return nil, err
	}

	paths := []string{}
	for _, file := range files {
		paths = append(paths, fmt.Sprintf("%s/%s", keyboardDirectory, file.Name()))
	}
	return paths, nil
}

func loadKeymapCollection(path string) (*Keyboard, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	var keyboard Keyboard
	err = json.Unmarshal(byteValue, &keyboard)
	if err != nil {
		return nil, err
	}

	keyboard.Slug = slugify.Marshal(keyboard.Name)
	return &keyboard, nil
}

// ----------------------------------------------------------------------------
// Creating a (JSON based) Repository
// ----------------------------------------------------------------------------
func NewJsonKeyboardRepository() (*JsonKeyboardRepository, error) {
	keymapPaths, err := loadKeymapFilenames()
	if err != nil {
		return nil, err
	}

	keyboards := map[string]*Keyboard{}
	names := []*KeyboardName{}
	for _, path := range keymapPaths {
		keyboard, err := loadKeymapCollection(path)
		if err != nil {
			return nil, err
		}
		names = append(names, &KeyboardName{
			Name: keyboard.Name,
			Slug: keyboard.Slug,
			Url:  fmt.Sprintf("/keyboards/%s", keyboard.Slug),
		})
		keyboards[keyboard.Slug] = keyboard
	}
	return &JsonKeyboardRepository{
		Keyboards:     keyboards,
		KeyboardNames: names,
	}, nil
}
