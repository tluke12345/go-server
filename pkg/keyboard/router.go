package keyboard

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
)

// ----------------------------------------------------------------------------
// Router
// ----------------------------------------------------------------------------
func NewKeyboardRouter(r KeyboardRepository) *chi.Mux {
	mux := chi.NewRouter()
	mux.Get("/", keyboardNamesHandler(r))
	mux.Get("/{slug}", keyboardHandler(r))
	return mux
}

// ----------------------------------------------------------------------------
// Handlers
// ----------------------------------------------------------------------------
func keyboardNamesHandler(r KeyboardRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		keyboardNames, err := r.GetKeyboardNames()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(keyboardNames)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func keyboardHandler(r KeyboardRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		slug := chi.URLParam(req, "slug")
		if slug == "" {
			http.Error(w, "missing name query parameter", http.StatusBadRequest)
			return
		}

		keymapCollection, err := r.GetKeyboard(slug)
		if err != nil {
			http.Error(w, err.Error(), http.StatusNotFound)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(keymapCollection)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}
