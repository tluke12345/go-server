package fileServer

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi/v5"
)

// FileServer conveniently sets up a http.FileServer handler to serve
func NewRouter(r chi.Router, route string, staticFilesDirectory string) {

  fmt.Println("Serving static files from: ", route)
  
  // Validate the route
  if strings.ContainsAny(route, "{}") {
    panic("FileServer does not permit any URL parameters.")
  }
  if len(route) < 2 || route[len(route)-1] != '*' || route[len(route)-2] != '/' {
    panic("FileServer route must end with /*")
  }

  // Create static file server
	dir, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	staticFiles := http.Dir(filepath.Join(dir, staticFilesDirectory))
	fs := http.FileServer(staticFiles)

  // Attach the file server to the router
  stripPrefix := strings.TrimSuffix(route, "*")
  r.Handle(route, http.StripPrefix(stripPrefix, fs))
}
