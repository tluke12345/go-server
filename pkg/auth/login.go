package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/tluke12345/go-server/web/templates"
)


// ----------------------------------------------------------------------------
// Login Form Response
// ----------------------------------------------------------------------------
type LoginResponse struct {
	Token string `json:"token"`
}


// ----------------------------------------------------------------------------
// Login Handlers
// ----------------------------------------------------------------------------

// GET Login
// HTML: login form page
func LoginPage(w http.ResponseWriter, r *http.Request) {
	template := templates.Login()
	err := template.Render(context.Background(), w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}


// POST Login
// Authorize user input and return jwt tokena
func Login(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user := r.PostForm.Get("username")
	pass := r.PostForm.Get("password")

	correctHash, err := HashedPasswordForUser(user)
	if err != nil {
		fmt.Println("Unauthorized user")
		http.Error(w, "Unauthorized user", http.StatusUnauthorized)
		return
	}

	isAuthorized := IsSamePassword(pass, correctHash)
	if !isAuthorized {
		fmt.Println("Unauthorized user")
		http.Error(w, "Unauthorized user", http.StatusUnauthorized)
		return
	}

	jwt, err := CreateJwtToken(user)
	if err != nil {
		fmt.Println("Error creating JWT", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(LoginResponse{ jwt })
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
