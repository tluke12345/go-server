package auth

import (
	"fmt"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
)


// ----------------------------------------------------------------------------
// Create jwt token for user
// valid for 24 hours from creation
// ----------------------------------------------------------------------------
func CreateJwtToken(username string) (string, error) {
	privateKeyFile, err := os.ReadFile(".keys/private_key.pem")
	if err != nil {
		return "", err
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyFile)
	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.RegisteredClaims{
		Subject: username,
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
		IssuedAt: jwt.NewNumericDate(time.Now()),
		NotBefore: jwt.NewNumericDate(time.Now()),
	})

	signedToken, err := token.SignedString(privateKey)
	if err != nil {
		return "", err
	}

	return signedToken, nil
}


// ----------------------------------------------------------------------------
// Verify jwt token
// Parse, validate, and retrieve claims
// ----------------------------------------------------------------------------
func VerifyJwtToken(tokenString string) (*jwt.RegisteredClaims, error) {
	file := ".keys/public_key.pem"
	publicKeyFile, err := os.ReadFile(file)
	if err != nil {
		return nil, err
	}

	publicKey, err := jwt.ParseRSAPublicKeyFromPEM(publicKeyFile)
	if err != nil {
		return nil, err
	}

	var claims = &jwt.RegisteredClaims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); ok {
			return publicKey, nil
		}
		return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
	})
	if err != nil {
		return nil, err
	}

	if !token.Valid {
		return nil, fmt.Errorf("Invalid Token\n")
	}
	return claims, nil
}
