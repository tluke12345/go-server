package auth

import "github.com/go-chi/chi/v5"

func AuthPagesRouter() chi.Router {
	r := chi.NewRouter()

	// Login/Logout
	r.Get("/login", LoginPage)
	r.Post("/login", Login)
	// r.Get("/logout", Logout)
	//
	// // Register New User
	// r.Get("/register", RegisterPage)
	// r.Post("/register", Register)

	return r
}
