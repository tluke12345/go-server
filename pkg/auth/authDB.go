package auth

import "fmt"

// Retrieve the hashed password for a given user
// returns 
//		string: hashed password
//		error: no password or db error
func HashedPasswordForUser(username string) (string, error) {
	// DB call here

	if username == "travis" {
		return "$2a$10$haUkAKnxpw7QXWfLLhdnQOYB7zbfk29rpLsrqWiZSPe.OXyLLALYC", nil
	}
	return "", fmt.Errorf("User not found")
}
