package auth

import (
	"context"
	"fmt"
	"net/http"
	"strings"
)

func ParseJWT(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		authHeaderStrings := strings.Split(authHeader, " ")

		if len(authHeaderStrings) != 2 {
			fmt.Println("Invalid Authorization Header: format")
			next.ServeHTTP(w, r)
			return
		}

		bearer := strings.ToLower(authHeaderStrings[0])
		if bearer != "bearer" {
			fmt.Println("Invalid Authorization Header: bearer tag")
			next.ServeHTTP(w, r)
			return
		}

		token := authHeaderStrings[1]
		claims, err := VerifyJwtToken(token)
		if err != nil {
			fmt.Println("Invalid JWT: verification failed")
			next.ServeHTTP(w, r)
			return
		}

		username := claims.Subject
		if username == "" {
			fmt.Println("Invalid JWT: missing username")
			next.ServeHTTP(w, r)
			return
		}

		// Update context
		ctx := context.WithValue(r.Context(), "username", username)

		// Authorization sucessful
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func EnsureAuthorization(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		username := r.Context().Value("username")
		if username == nil {
			http.Error(w, "Not Authorized", http.StatusUnauthorized)
			return
		}

		ctx := context.WithValue(r.Context(), "isAuthorized", true)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
