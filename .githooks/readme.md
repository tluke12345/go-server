# Git Hooks

the .git/hooks directory is not part of the repository, so it is not pushed
with the rest of the code. This means that each developer must create them.

Instead, we can create an .git-external directory with our hooks and tell git
to use them. This way, we can share the hooks with the rest of the team.

## How to use

1. run the script ./scripts/setup.sh, to configure git to use the hooks
   in the .git-external directory.
