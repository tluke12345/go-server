module gitlab.com/tluke12345/go-server

go 1.21

toolchain go1.21.8

require (
	github.com/a-h/templ v0.2.648
	github.com/go-chi/chi/v5 v5.0.12
	github.com/metal3d/go-slugify v0.0.0-20160607203414-7ac2014b2f23
	golang.org/x/crypto v0.22.0
)

require github.com/golang-jwt/jwt/v5 v5.2.1
